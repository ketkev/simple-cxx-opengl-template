// clang-format off
// This is necessary because glew has to be included before freeglut
#include <GL/glew.h>
#include <GL/freeglut.h>
// clang-format on

const int WIDTH = 800, HEIGHT = 600;

void Render() {
  glClearColor(0.0f, 0.0f, 0.4f, 1.0f);
  glClear(GL_COLOR_BUFFER_BIT);

  glFlush();
}

int main(int argc, char** argv) {
  glutInit(&argc, argv);
  glutInitWindowSize(WIDTH, HEIGHT);
  glutInitWindowPosition(50, 50);
  glutCreateWindow("OpenGL");
  glutDisplayFunc(Render);

  glutMainLoop();

  return 0;
}
